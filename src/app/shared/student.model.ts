export class Student {
  constructor (public studentId: number, public firstName: string, public lastName: string, public email: string) {}
}
