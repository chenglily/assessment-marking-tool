import {Student} from "./student.model";

export class AssessmentDetail {
  constructor (public name: string, public student: Student[]) {}
}
