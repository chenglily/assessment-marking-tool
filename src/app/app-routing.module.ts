import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CourseComponent} from "./course/course.component";
import {CourseStartComponent} from "./course/course-start/course-start.component";
import {AssessmentComponent} from "./course/assessment/assessment.component";
import {CourseEditComponent} from "./course/course-edit/course-edit.component";
import {TutorComponent} from "./tutor/tutor.component";
import {SignupComponent} from "./auth/signup/signup.component";
import {SigninComponent} from "./auth/signin/signin.component";
import {AuthGuard} from "./auth/auth-guard.service";
import {StudentComponent} from "./student/student.component";
import {TutorNewComponent} from "./tutor/tutor-new/tutor-new.component";
import {TutorPersonComponent} from "./tutor/tutor-person/tutor-person.component";
import {StudentListComponent} from "./student-list/student-list.component";
import {TutorDetailComponent} from "./tutor/tutor-detail/tutor-detail.component";
import {TutorListComponent} from "./tutor/tutor-list/tutor-list.component";

const  appRoutes: Routes = [
  //Guard when finished
  // canActivate: [AuthGuard],
  { path: '', component: SigninComponent},
  { path: 'course', component: CourseComponent, canActivate: [AuthGuard], children: [
      { path: '', component: CourseStartComponent},
      { path: 'new', component: CourseEditComponent},
      { path: ':id', component: AssessmentComponent},
      { path: ':id/edit', component: CourseEditComponent},
    ]},
  { path: 'tutor', component: TutorComponent, canActivate: [AuthGuard], children: [
      { path: '', component: TutorListComponent},
      { path: 'new', component: TutorNewComponent},
      // { path: 'edit', component: TutorEditComponent},
      { path: ':id', component: TutorPersonComponent},
      // { path: ':id/edit', component: TutorEditComponent},
    ]},
  { path: 'signup', component: SignupComponent},
  { path: 'signin', component: SigninComponent},
  { path: 'student', component: StudentComponent, canActivate: [AuthGuard]},
  { path: 'student-list', component: StudentListComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
