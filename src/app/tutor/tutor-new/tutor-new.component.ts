import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TutorService} from "../tutor.service";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-tutor-new',
  templateUrl: './tutor-new.component.html',
  styleUrls: ['./tutor-new.component.css']
})
export class TutorNewComponent implements OnInit {
  tutorForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tutorService: TutorService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.initForm();
  }

  onSubmit() {
    //To create new users, Working fine
    this.authService.signupUser(this.tutorForm.value.email, this.tutorForm.value.password);
    //Create a new tutor and send to Firebase
    this.tutorService.createTutor(this.tutorForm.value);
    this.onCancel();
  }

  private initForm() {
    let firstName = '';
    let lastName = '';
    let email = '';
    let password = '';

    this.tutorForm = new FormGroup({
      'firstName': new FormControl(firstName, Validators.required),
      'lastName': new FormControl(lastName, Validators.required),
      'email': new FormControl(email, [Validators.required, Validators.email]),
      'password': new FormControl(password, [Validators.required, Validators.minLength(6)]),
    });
  }

  onCancel() {
    this.router.navigate(['/tutor']);
  }
}
