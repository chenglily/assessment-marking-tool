import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFireDatabase} from "angularfire2/database";
import {TutorService} from "../tutor.service";
import {Tutor} from "../tutor.model";

@Component({
  selector: 'app-tutor-detail',
  templateUrl: './tutor-detail.component.html',
  styleUrls: ['./tutor-detail.component.css']
})
export class TutorDetailComponent implements OnInit {
  @Input() tutor: any;
  @Input() index: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tutorService: TutorService,
              private db: AngularFireDatabase) {
  }

  ngOnInit() {
  }

  onEditTutor(tutor: Tutor) {
    this.tutorService.selectedTutor = Object.assign({}, tutor);
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteTutor() {
    this.tutorService.deleteSelectedTutor(this.tutor.key);
    this.router.navigate(['/tutor']);
  }

}
