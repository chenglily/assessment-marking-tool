import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {TutorService} from "../tutor.service";
import {Course} from "../../course/course.model";
import {Observable} from "rxjs/Observable";
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'app-tutor-person',
  templateUrl: './tutor-person.component.html',
  styleUrls: ['./tutor-person.component.css']
})
export class TutorPersonComponent implements OnInit {
  tutor: Observable<any>;
  course: Course;
  id: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tutorService: TutorService,
              private db: AngularFireDatabase) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          console.log('Tutor index: ' + this.id);
         console.log( this.db.object('tutor/:id').valueChanges());
        }
      );
    this.getTutor();
  }

  getTutor() {
    this.db.object('tutor/:id').snapshotChanges();
    console.log(this.db.object('tutor/:id').snapshotChanges());
  }

  onEditTutor() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  // onDeleteTutor() {
  //   // this.tutorService.deleteTutor(this.id);
  //   // this.tutorService.deleteSelectedTutor(tutorUsername);
  //   this.router.navigate(['/tutor']);
  // }

}
