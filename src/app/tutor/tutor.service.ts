import {Injectable} from "@angular/core";
import {Tutor} from "./tutor.model";
import {AngularFireDatabase, AngularFireList, AngularFireObject} from "angularfire2/database";

@Injectable()
export class TutorService {

  tutorPerson: AngularFireObject<Tutor> = null;
  tutorRef: AngularFireList<Tutor> = null;

  selectedTutor: Tutor = new Tutor();
  private basePath: string = '/tutors';

  constructor(private db: AngularFireDatabase) {
    this.tutorRef = db.list(this.basePath);
  }

  //Getting all tutors
  getTutors(): AngularFireList<Tutor> {
    return this.tutorRef;
  }

  //Getting selected tutor
  getEachTutor(key: string): AngularFireObject<Tutor> {
    const tutorPath = '${this.basePath}/${key}';
    this.tutorPerson = this.db.object(tutorPath);
    return this.tutorPerson;
  }

  //Creating a new tutor and send to Firebase
  createTutor(tutor: Tutor): void {
    this.tutorRef.push(tutor);
  }

  updateSelectedTutor(key: string, value: any): void {
    this.tutorRef.update(key, value).catch(error => this.handleError(error));
  }

  deleteSelectedTutor(key: string): void {
    this.tutorRef.remove(key).catch(error => this.handleError(error));
  }

  private handleError(error) {
    console.log(error);
  }

}

