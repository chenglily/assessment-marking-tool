import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {TutorService} from "../tutor.service";
import 'rxjs/Rx';

@Component({
  selector: 'app-tutor-list',
  templateUrl: './tutor-list.component.html',
  styleUrls: ['./tutor-list.component.css']
})
export class TutorListComponent implements OnInit {
  tutors: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private tutorService: TutorService) { }

  ngOnInit() {
    this.getTutorList();
  }

  //Convert Observable to an array
  getTutorList() {
    // Use snapshotChanges().map() to store the key
    this.tutorService.getTutors().snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(tutors => {
      this.tutors = tutors;
    });
  }

  onAddTutor() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
