import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CourseComponent } from './course/course.component';
import { AssessmentComponent } from './course/assessment/assessment.component';
import {CourseItemComponent} from './course/course-item/course-item.component';
import { CourseDetailComponent } from './course/course-item/course-detail/course-detail.component';
import {AppRoutingModule} from "./app-routing.module";
import { CourseStartComponent } from './course/course-start/course-start.component';
import { CourseEditComponent } from './course/course-edit/course-edit.component';
import {DropdownDirective} from "./shared/dropdown.directive";
import { TutorComponent } from './tutor/tutor.component';
import {CourseService} from "./course/course.service";
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import {AuthService} from "./auth/auth.service";
import {DataStorageService} from "./shared/data-storage.service";
import {AuthGuard} from "./auth/auth-guard.service";
import {HttpClientModule} from "@angular/common/http";
import { StudentComponent } from './student/student.component';
import {StudentListService} from "./student-list/student-list.service";
import { TutorListComponent } from './tutor/tutor-list/tutor-list.component';
import { TutorDetailComponent } from './tutor/tutor-detail/tutor-detail.component';
import {TutorService} from "./tutor/tutor.service";
import { TutorPersonComponent } from './tutor/tutor-person/tutor-person.component';
import { StudentListComponent } from './student-list/student-list.component';
import {environment} from "../environments/environment";
import {AngularFireModule} from "angularfire2";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {TutorNewComponent} from "./tutor/tutor-new/tutor-new.component";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CourseComponent,
    AssessmentComponent,
    CourseItemComponent,
    CourseDetailComponent,
    CourseStartComponent,
    CourseEditComponent,
    DropdownDirective,
    TutorComponent,
    SignupComponent,
    SigninComponent,
    StudentComponent,
    TutorListComponent,
    TutorDetailComponent,
    TutorPersonComponent,
    StudentListComponent,
    TutorNewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [CourseService, AuthService, DataStorageService, AuthGuard, StudentListService, TutorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
