import {Component, OnInit} from '@angular/core';
import {Student} from "../shared/student.model";

import {StudentListService} from "../student-list/student-list.service";
import {Subscription} from "rxjs/Subscription";
import {DataStorageService} from "../shared/data-storage.service";
import {HttpEvent} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  students: Student[];
  studentsObservable: Observable<any[]>;

  constructor(private studentListService: StudentListService,
              private dataStorageService: DataStorageService,
              private db: AngularFireDatabase) { }

  ngOnInit() {
    this.studentsObservable = this.getStudents();
  }

  getStudents() {
    return this.db.list('/students').valueChanges();
  }

  onSaveStudent() {
    this.dataStorageService.storeStudents()
      .subscribe(
        (response: HttpEvent<object>) => {
          console.log(response)
        }
      );
  }



}
