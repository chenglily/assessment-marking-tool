import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CourseService} from "./course.service";

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  coursesObservable: Observable<any[]>;

  constructor(private courseService: CourseService) {}

  ngOnInit() {
    this.coursesObservable = this.getCourses();
  }

  getCourses() {
    return this.courseService.getCourses();
  }
}
