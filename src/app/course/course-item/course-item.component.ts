import {Component, Input, OnInit} from '@angular/core';
import {Course} from '../course.model';
import {CourseService} from '../course.service';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css']
})
export class CourseItemComponent implements OnInit {
  courses: Course[];
  @Input() studentsObservable: Observable<any[]>;

  constructor(private courseService: CourseService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.courseService.coursesChanged
      .subscribe(
        (courses: Course[]) => {
          this.courses = courses;
        }
      );
    this.courses = this.courseService.getCoursesList();
    this.studentsObservable = this.courseService.getCourses();
  }

  onNewCourse() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
