import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {CourseService} from "../course.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpEvent} from "@angular/common/http";
import {DataStorageService} from "../../shared/data-storage.service";

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit {
  id: number;
  editMode = false;
  courseForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private courseService : CourseService,
              private router: Router,
              private dataStorageService: DataStorageService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;
          this.initForm();
        }
      );
  }

  onSubmit() {
    if (this.editMode) {
      this.courseService.updateCourse(this.id, this.courseForm.value);
      this.onSaveCourse();
    } else {
      this.courseService.addCourse(this.courseForm.value);
      this.onSaveCourse();
    }
    this.onCancel();
  }

  onSaveCourse() {
    this.dataStorageService.storeCourses()
      .subscribe(
        (response: HttpEvent<object>) => {
          console.log(response)
        }
      );
  }

  private initForm() {
    let courseId = '';
    let courseName = '';
    let location = '';
    let term = '';
    let courseAssessment = new FormArray([]);
    let courseStudent = new FormArray([]);

    if (this.editMode) {
      const course = this.courseService.getCourse(this.id);
      courseId = course.id;
      courseName = course.name;
      location = course.location;
      term = course.term;

      //In case assessment array is empty
      if (course['assessments']) {
        for (let assessment of course.assessments) {
          courseAssessment.push(
            new FormGroup({
              'name': new FormControl(assessment.name, Validators.required)
            })
          );
        }
      }

      //In case student array is empty
      // if (course['students']) {
      //   for (let student of course.students) {
      //     courseStudent.push(
      //       new FormGroup({
      //         'studentId': new FormControl(student.studentId, Validators.required),
      //         'firstName': new FormControl(student.firstName, Validators.required),
      //         'lastName': new FormControl(student.lastName, Validators.required),
      //         'email': new FormControl(student.email, Validators.required)
      //       })
      //     );
      //   }
      // }
    }

    this.courseForm = new FormGroup({
      'id': new FormControl(courseId, Validators.required),
      'name': new FormControl(courseName, Validators.required),
      'location': new FormControl(location, Validators.required),
      'term': new FormControl(term, Validators.required),
      'assessments': courseAssessment,
      'students': courseStudent
    });
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  onDeleteAssessment(index: number) {
    (<FormArray>this.courseForm.get('assessments')).removeAt(index);
  }

  onAddAssessment() {
    (<FormArray>this.courseForm.get('assessments')).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required)
      })
    );
  }
}
