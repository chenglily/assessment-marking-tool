import {Course} from './course.model';
import {Subject} from "rxjs/Subject";
import {Student} from "../shared/student.model";
import {StudentListService} from "../student-list/student-list.service";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {AngularFireDatabase} from "angularfire2/database";
import {FirebaseListObservable, FirebaseObjectObservable} from "angularfire2/database-deprecated";

@Injectable()
export class CourseService {
  coursesChanged = new Subject<Course[]>();

  courseList: FirebaseListObservable<Course[]> = null;
  eachCourse: FirebaseObjectObservable<Course> = null;

  private basePath: string = '/courses';

  private courses: Course[] = [
    // new Course('ITECH 1000', 'Programming One', 'Mt Helen', 'Semester 1 2018',
    //   [new AssessmentDetail( 'Ticket System', [new Student(300001, 'Alice', 'Williams', 'alice@gmail.com'),
    //     new Student(300002, 'James', 'King', 'james@gmail.com')]),
    //     new AssessmentDetail('Club management', [new Student(300001, 'Alice', 'Williams', 'alice@gmail.com'),
    //       new Student(300002, 'James', 'King', 'james@gmail.com')])],
    // ),
    // new Course('ITECH 1006', 'Database', 'SMB', 'Semester 1 2019',
    //   [new AssessmentDetail( 'ERD', [new Student(300003, 'Alice', 'Morris', 'alice@gmail.com'),
    //     new Student(300004, 'Kate', 'Wood', 'kate@gmail.com')]),
    //     new AssessmentDetail('SQL', [new Student(300003, 'Alice', 'Morris', 'alice@gmail.com'),
    //       new Student(300004, 'Kate', 'Wood', 'kate@gmail.com')])],
    //   ),
    // new Course('ITECH 5500', 'Communication', 'Gippsland', 'Semester 2 2019',
    //   [new AssessmentDetail('Literature Review',  [new Student(300005, 'Pat', 'Bell', 'pat@gmail.com'),
    //     new Student(300006, 'Berry', 'Scott', 'berry@gmail.com')]),
    //     new AssessmentDetail( 'Presentation',  [new Student(300005, 'Pat', 'Bell', 'pat@gmail.com'),
    //       new Student(300006, 'Berry', 'Scott', 'berry@gmail.com')])],
    //  )
  ];

  constructor(private studentService: StudentListService,
              private db: AngularFireDatabase) {
  }

  //Getting all the courses
  getCourses():  FirebaseListObservable<Course[]> {
    this.courseList = (<FirebaseListObservable<Course[]>>this.db.list(this.basePath).valueChanges());
    return this.courseList;
  }

  //Getting selected course
  getEachCourse(index: number): FirebaseObjectObservable<Course> {
    const coursePath =  `${this.basePath}/${index}`;
    this.eachCourse = (<FirebaseObjectObservable<Course>>this.db.object(coursePath).valueChanges());
    return this.eachCourse;
  }


  //Getting all the courses
  //Deprecated
  getCoursesList() {
    return this.courses.slice();
  }

  //Get one course according to the index
  //Deprecated
  getCourse(index: number) {
    return this.courses[index];
  }

  toStudentList(students: Student[]) {
    this.studentService.addStudents(students, true);
  }

  updateCourse(courseIndex: number, newCourse: Course) {
    this.courses[courseIndex] = newCourse;
    this.coursesChanged.next(this.courses.slice());
  }

  addCourse(course: Course) {
    this.courses.push(course);
    this.coursesChanged.next(this.courses.slice());
  }

  deleteCourse(index: number) {
    this.courses.splice(index, 1);
    this.coursesChanged.next(this.courses.slice());
  }

}
