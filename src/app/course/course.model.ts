import {AssessmentDetail} from '../shared/assessment-detail.model';
import {Student} from "../shared/student.model";

export class Course {
  public id: string;
  public name: string;
  public location: string;
  public term: string;
  public assessments: AssessmentDetail[];
  // public students: Student[];

  constructor (id: string, name: string, location: string, term: string, assessments: AssessmentDetail[]) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.term = term;
    this.assessments = assessments;
    // this.students = students;
  }
}
