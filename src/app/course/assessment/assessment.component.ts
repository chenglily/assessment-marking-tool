import {Component, OnInit} from '@angular/core';
import {Course} from '../course.model';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {CourseService} from "../course.service";
import {Student} from "../../shared/student.model";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.css']
})
export class AssessmentComponent implements OnInit {
  course: Course;
  id: number;
  students: Student[];
  student: Student;
  courseObservable: Observable<any>;

  constructor(private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          console.log('Course index: ' + this.id);
          this.course = this.courseService.getCourse(this.id);
          this.courseObservable = this.courseService.getEachCourse(this.id);
        }
      );
  }

  onEditCourse() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteCourse() {
    this.courseService.deleteCourse(this.id);
    this.router.navigate(['/course']);
  }

  onStudent(index: number) {
    //take the student list
    // this.courseService.toStudentList(this.course.students);
    // this.students = this.courseService.getCourse(this.id).assessments[index].student;
    this.students = this.course.assessments[index].student;
    console.log(this.students);
    this.courseService.toStudentList(this.students);
    this.router.navigate(['/student-list'], {relativeTo: this.route});
  }

}
