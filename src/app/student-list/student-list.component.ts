import { Component, OnInit } from '@angular/core';
import {Student} from "../shared/student.model";
import {Subscription} from "rxjs/Subscription";
import {StudentListService} from "./student-list.service";

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  students: Student[];
  private subscription: Subscription;

  constructor(private studentService: StudentListService) { }

  ngOnInit() {
    this.students = this.studentService.getStudents();
    this.subscription = this.studentService.studentsChanged
      .subscribe(
        (students: Student[]) => {
          this.students = students;
        }
      );
  }

}
