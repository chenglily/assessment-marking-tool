import {Student} from "../shared/student.model";
import {Subject} from "rxjs/Subject";
import {Injectable} from "@angular/core";

@Injectable()
export class StudentListService {
  studentsChanged = new Subject<Student[]>();


  private students: Student[] = [
    // new Student(3000012, 'Michael', 'Jackson', 'michael@gamil.com'),
    // new Student(3000013, 'May', 'Jobs', 'may@gamil.com'),
    // new Student(300001, 'Alice', 'Williams', 'alice@gmail.com'),
    // new Student(300002, 'James', 'King', 'james@gmail.com'),
    // new Student(300003, 'Alice', 'Morris', 'alice@gmail.com'),
    // new Student(300004, 'Kate', 'Wood', 'kate@gmail.com'),
    // new Student(300005, 'Pat', 'Bell', 'pat@gmail.com'),
    // new Student(300006, 'Berry', 'Scott', 'berry@gmail.com')

  ];

  getStudents() {
    return this.students.slice();
  }

  addStudents(students: Student[], clear: boolean) {
    if (clear) {
      this.clearStudents()
    }
    this.students.push(...students);
    this.studentsChanged.next(this.students.slice());
  }

  clearStudents() {
    this.students = [];
  }

  setStudents(students: Student[]) {
    this.students = students;
    this.studentsChanged.next(this.students.slice());
  }


}
